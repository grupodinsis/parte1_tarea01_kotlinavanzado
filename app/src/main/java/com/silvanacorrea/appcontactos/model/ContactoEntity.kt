package com.silvanacorrea.appcontactos.model

import java.io.Serializable

data class ContactoEntity(val id: Int, val nombre: String, val telefono: String, val email: String, val foto: Int) : Serializable
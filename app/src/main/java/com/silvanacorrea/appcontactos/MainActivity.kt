package com.silvanacorrea.appcontactos

import android.os.Bundle
import com.silvanacorrea.appcontactos.contactos.ContactosActivity


class MainActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

       next(ContactosActivity::class.java, null,true)

    }
}
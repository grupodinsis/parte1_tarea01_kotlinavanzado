package com.silvanacorrea.appcontactos.contactos

import android.os.Bundle
import android.content.Context
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.silvanacorrea.appcontactos.R
import com.silvanacorrea.appcontactos.model.ContactoEntity
import kotlinx.android.synthetic.main.fragment_contactos_detalle.*
import kotlinx.android.synthetic.main.row_contacto.*
import java.lang.RuntimeException

class ContactosDetalleFragment : Fragment() {

    private var listener: OnContactoListener? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_contactos_detalle, container, false)
    }

    fun renderContact(contactosEntity: ContactoEntity){
        val nombre   = contactosEntity.nombre
        val telefono = contactosEntity.telefono
        val email    = contactosEntity.email

        tviNombre.text = nombre
        tviTelefono.text = telefono
        tviEmail.text = email

        iviContacto.setImageResource(contactosEntity.foto)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if(context is OnContactoListener){
            listener = context
        } else{
            throw RuntimeException("$context necesita implementar OnContactoListener")
        }
    }

    override fun onDetach() {
        super.onDetach()

        listener = null
    }


}
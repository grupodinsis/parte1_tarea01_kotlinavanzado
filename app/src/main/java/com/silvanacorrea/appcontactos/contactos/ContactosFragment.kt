package com.silvanacorrea.appcontactos.contactos

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.silvanacorrea.appcontactos.R
import com.silvanacorrea.appcontactos.adapter.ContactoAdapter
import com.silvanacorrea.appcontactos.model.ContactoEntity
import kotlinx.android.synthetic.main.fragment_contactos.*



class ContactosFragment : Fragment() {

    private var listener: OnContactoListener? = null
    private var contactos = mutableListOf<ContactoEntity>()

      override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_contactos, container, false)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnContactoListener){
            listener = context
        }else{
            throw RuntimeException("$context necesita implemetar OnContactoListener")
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setData()

        lstContactos.adapter = ContactoAdapter(requireContext(),contactos)

        lstContactos.setOnItemClickListener {_, _, i, _->
            listener?.let {
                it.selectedItemContact(contactos[i])
            }
        }

        listener?.renderFirst(first())
    }

    private fun setData(){
        val contacto1 = ContactoEntity(1,"Silvana","978371428","silvana@gmail.com", R.mipmap.img001)
        val contacto2 = ContactoEntity(2, "José", "954743311","jose@gmail.com", R.mipmap.img002)
        val contacto3 = ContactoEntity(2, "Jhoisy", "978163456","jhoisy@gmail.com", R.mipmap.img003)

        contactos.add(contacto1)
        contactos.add(contacto2)
        contactos.add(contacto3)
    }

    private fun first(): ContactoEntity? = contactos?.first()

    override fun onDetach() {
        super.onDetach()
        listener = null
    }


}
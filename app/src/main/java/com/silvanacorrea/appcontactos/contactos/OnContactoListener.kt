package com.silvanacorrea.appcontactos.contactos

import com.silvanacorrea.appcontactos.model.ContactoEntity

interface OnContactoListener {

    fun selectedItemContact(contactoEntity: ContactoEntity)
    fun renderFirst(contactoEntity: ContactoEntity?)
}
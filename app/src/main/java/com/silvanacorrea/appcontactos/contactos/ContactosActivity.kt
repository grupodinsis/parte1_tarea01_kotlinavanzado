package com.silvanacorrea.appcontactos.contactos

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.FragmentManager
import com.silvanacorrea.appcontactos.model.ContactoEntity
import com.silvanacorrea.appcontactos.R

class ContactosActivity : AppCompatActivity(), OnContactoListener {

    private lateinit var contactosFragment: ContactosFragment
    private lateinit var contactosDetalleFragment: ContactosDetalleFragment
    private lateinit var fragmentManager: FragmentManager

    override fun selectedItemContact(contactoEntity: ContactoEntity) {

        contactosDetalleFragment.renderContact(contactoEntity)

    }

    override fun renderFirst(contactoEntity: ContactoEntity?) {
        contactoEntity?.let {
            selectedItemContact(it)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_contactos)

        fragmentManager = supportFragmentManager
        contactosFragment = fragmentManager.findFragmentById(R.id.fragContactos) as ContactosFragment
        contactosDetalleFragment = fragmentManager.findFragmentById(R.id.fragContactosDetalle) as ContactosDetalleFragment
    }


}
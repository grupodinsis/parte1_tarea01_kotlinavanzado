package com.silvanacorrea.appcontactos.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import com.silvanacorrea.appcontactos.R
import com.silvanacorrea.appcontactos.model.ContactoEntity


class ContactoAdapter (private val context: Context, private val contactos: List<ContactoEntity>): BaseAdapter(){

    override fun getItem(position: Int) = contactos[position]

    override fun getItemId(position: Int): Long = 0

    override fun getCount(): Int = contactos.size

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val inflater = LayoutInflater.from(context)
        val container = inflater.inflate(R.layout.row_contacto, null)
        val imgContacto = container.findViewById<ImageView>(R.id.iviContact)
        val tviName = container.findViewById<TextView>(R.id.tviNombr)

        val contactoEntity = contactos[position]

        tviName.text = contactoEntity.nombre
        imgContacto.setImageResource(contactoEntity.foto)

        return container

    }


}